### Hi there 👋

I'm Nico! I'm a passionate software & machine learning engineer with a strong focus on standards, best practices and architecture. 😎

I love working on challenging projects, which allow me to combine both of my chosen fields to create scalable and intelligent software solutions. 🤖

Also a Linux nerd. 🤓

I do *not-so-regular-but-still* [dev blogging about various topics](https://www.auroria.io), sometimes I also participate in [kaggle competitions](https://www.kaggle.com/niggoo). ⌨️

If you wanna connect, hit me up on [Mastodon](https://mastodon.social/@niggoo). 🐘 📢
